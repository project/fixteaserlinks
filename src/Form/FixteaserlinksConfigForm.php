<?php

/**
 * @file
 * Contains \Drupal\fixteaserlinks\Form\FixteaserlinksConfigForm.
 */

namespace Drupal\fixteaserlinks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class FixteaserlinksConfigForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fixteaserlinks_config_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [
      'pcomment' => 'Add new comment/Log in to post comments',
      'comcount' => 'X comments (comment count)',
      'readmore' => 'Read more',
      'hnnsblog' => "NN's blog",
    ];
    $pcommentp = \Drupal::config('fixteaserlinks.settings')->get('fixteaserlinks_comment');
    $comcountp = \Drupal::config('fixteaserlinks.settings')->get('fixteaserlinks_comcount');
    $readmorep = \Drupal::config('fixteaserlinks.settings')->get('fixteaserlinks_readmore');
    $hnnsblogp = \Drupal::config('fixteaserlinks.settings')->get('fixteaserlinks_nnsblog');
    $settings = [
      'pcomment' => $pcommentp ? 'pcomment' : FALSE,
      'comcount' => $comcountp ? 'comcount' : FALSE,
      'readmore' => $readmorep ? 'readmore' : FALSE,
      'hnnsblog' => $hnnsblogp ? 'hnnsblog' : FALSE,
    ];
    $form['fixteaserlinks_config'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#title' => t('Disable the following teaser links:'),
      '#options' => $options,
      '#default_value' => $settings,
      '#description' => t('To disable a link, check the box to the left of the item.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save configuration',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('fixteaserlinks.settings')->set('fixteaserlinks_comment', $form_state->getValue(['fixteaserlinks_config', 'pcomment']))->save();
    \Drupal::configFactory()->getEditable('fixteaserlinks.settings')->set('fixteaserlinks_comcount', $form_state->getValue(['fixteaserlinks_config', 'comcount']))->save();
    \Drupal::configFactory()->getEditable('fixteaserlinks.settings')->set('fixteaserlinks_readmore', $form_state->getValue(['fixteaserlinks_config', 'readmore']))->save();
    \Drupal::configFactory()->getEditable('fixteaserlinks.settings')->set('fixteaserlinks_nnsblog', $form_state->getValue(['fixteaserlinks_config', 'hnnsblog']))->save();
    $this->messenger()->addMessage(t('Configuration saved.'));
  }

}
?>
