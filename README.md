# Fix Teaserlinks

## Contents of this file

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainer


## Introduction

The “Fix Teaserlinks” module is a simple module that may be used to
manage the visibility of the links that appear below teasers.

It is intended for site builders who want a cleaner look of teaser
lists.

* For a full description of the module, visit the [project page][1].

* To submit bug reports and feature suggestions, or to track changes
  visit the project's [issue tracker][2].


## Recommended modules

* [Advanced Help Hint][3]:  
  To suggest that Advanced Help is should be installed for online help.

* [Advanced Help][4]:  
  When this module is enabled, the project's `README.md` will be
  shown when you visit  
  `help/ah/fixteaserlinks/README.md`.

# Installation

1. Install as you would normally install a contributed drupal
   module. See: [Installing Drupal 8 modules][5] (D8) for further
   information.

2. Enable the “Fix Teaserlinks” module on the Modules list page in the
   administrative interface.

## Configuration

By default, the module will not do anything until you configure it.

To configure, navigate to **Manage » Configuration » System » Fix
Teaserlinks**.

There are four checkboxes:

1. The first will remove the links “Add new comment” for logged in
   users, and “Log in to post comments” for those that are not logged
   in.

2. The second will remove the comment count.

3. The third will remove the “Read more” link.

4. The last will remove the link to the author's blog.

Note that these links does not always appear.  The links that appear
(unless hidden by this module) depends on: The theme, the template
for the content type, and the modules enabled.

To cancel all these effects and delete the variables that retain the
settings, uninstall the module and clear caches.

## Maintainer

* [gisle](https://www.drupal.org/u/gisle)

[1]: https://www.drupal.org/project/fixteaserlinks
[2]: https://www.drupal.org/project/issues/fixteaserlinks
[3]: https://www.drupal.org/project/advanced_help_hint
[4]: https://www.drupal.org/project/advanced_help
[5]: https://www.drupal.org/node/1897420
